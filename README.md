# Dokumentace k EverydayNotes

## Cíl projektu
Cílem projektu bylo vytvořit jednoduchou aplikaci pro ukládání poznámek a jejich případnou úpravu textu. A naučit se tím základní možnosti, které nabízí javascript pro tvorbu single page aplikace.
Postup
Práce je polepena z vlastních znalostí, znalostí ze cvičení a kódů ze cvik a případnými návody.
Nejdříve jsem vytvořil základ statické stránky pomocí html a základních css prvků a poté jsem převáděl daný kód do jednotlivých funkčních/objektových celků, které jsem tvořil pomocí javascriptu a následně využíval. 

## Popis
Aplikace je velice jednoduchá, v pravém horním rohu je tlačítko +, které následně otevře formulář pro vyplnění dané poznámky, ke které je nutné napsat text a následně je možné přiložit datum a odkaz na video – původně bylo zamýšleno přidat celé video, jenže s tím youtube dělal akorát potíže.

Po vytvoření poznámky jsme zpátky na hlavní stránce, kde můžeme vidět všechny naše poznámky.
Jednotlivé poznámky pomocí dvojkliku jsme schopni otevřít a následně případně editovat dané textové pole v nich. A v případě, že bychom chtěli poznámku smazat, je to možné pomocí hoveru nad poznámkou a následného stisku klávesy „delete“.
Další možností je přesouvání/prohazování jednotlivých poznámek mezi sebou.
Následně v dolním levém rohu je tlačítko pro zapínání/vypínání hudby. V případě povolení je celá stránka doprovázena background hudbou a dále přidání (finální submit) a smazání poznámky je také obohaceno o zvuk.

Problém naskytne, pokud přidáte novou poznámku a s ní chcete drag and dropovat, problém je, že nemá navázaný posluchač na sebe. Pomůže reload stránky, což mi ale přišlo nefektivní a zároveň díky  tomu nemůže zachovávat uživatelu preferenci o puštěné hudbě. 

## Obsažené funkčnosti
V mém kódu by měly být pokryty všechny body, které jsou vyzeleněné na následujícím obrázku.
- Grafika – svg je ikonka stránky, + tlačítko, music button a také copyright.
- Média – jak bylo zmíněno, stránka je obohacena o background hudbu a doplnění zvuků u přidání a smazání poznámky.
- Formuláře – přidávání nové poznámky je formulář obsahující textové pole, pole s datumem a pole pro vložení url.
- Offline aplikace – aplikace má v horním pravém rohu ukazatel připojení, který se mění v zelený/červený v závislosti na připojení
- Pokročilé selektory – css obsahují selektory hover, nth child, before, after, focus, typy inputů, …
- Vendor prefixy – pro některé animace/pozicování jsem využil vendor prefixů v css
- CSS3 – využití tranlate/scale/rotate pro pozicování jednotlivých elementů, a především tlačítek a jejich následná případná animace především pomocí keyframů, ale i transitions.
- Media queries – jednoduché zmenšení nadpisů/buttonu pro menší zařízení
- OOP přístup – kód je členěn do jednotlivých modulů, které většinou obsahují třídu s metodami a proměnnými.
- JS API – využil jsem localStorage pro ukládání jednotlivých poznámek a poté Drag N Drop pro přesun poznámek po obrazovce na určená místa.
- Ovládání médií – na základě uživatelské preference se zapíná/vypíná zvukový doprovod stránky
- Offline aplikace – viz výše
- JS práce s SVG – případná úprava svg a změna barev, dokreslení překřížení

![Excel file](excel.png)
