// Import the required modules
import {Notes} from './modules/notes.js';
import {Connection} from './modules/connectionAPI.js';
import {NotesManager} from './modules/draganddropAPI.js';
import {Audio} from './modules/audioAPI.js';


// Create an instance of the Connection class to manage the connection
const connection = new Connection();

// Update the status of the connection
connection.updateStatus();

// Create an instance of the Notes class to manage notes
const notes = new Notes();

// Create an instance of the NotesManager class to manage drag-and-drop functionality
const notesManager = new NotesManager();