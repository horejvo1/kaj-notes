// Exporting Connection class
export class Connection {
    statusElement;

    // The constructor is called when a new instance of the class is created
    constructor() {
        // Get the HTML element with id "status"
        this.statusElement = document.getElementById("status");

        // Add an event listener for the 'online' event to the window object
        // When the 'online' event is fired, it calls updateStatus function with 'online' as parameter
        window.addEventListener('online', () => this.updateStatus('online'));

        // Add an event listener for the 'offline' event to the window object
        // When the 'offline' event is fired, it calls updateStatus function with 'offline' as parameter
        window.addEventListener('offline', () => this.updateStatus('offline'));
    }

    // Method to update the status of the connection
    updateStatus(status) {
        // Check if the browser is online
        if (navigator.onLine) {
            this.statusElement.textContent = 'You are online';
            this.statusElement.style.color = 'limegreen';
        } else {
            this.statusElement.textContent = 'You are offline';
            this.statusElement.style.color = 'red';
        }
    }
}