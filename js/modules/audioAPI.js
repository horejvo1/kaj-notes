// Exporting Audio class
export class Audio {
    // The constructor is called when a new instance of the class is created
    constructor() {
        this.bgMusic = document.querySelector('#audio_bg');
        this.addSound = document.querySelector('#audio_add');
        this.deleteSound = document.querySelector('#audio_delete');
        this.muteButton = document.querySelector('#audio-button');
        this.crossButton = document.querySelector('#audio-crossed');

        // Attach an event listener to the mute button to toggle the mute status when clicked
        this.muteButton.addEventListener('click', () => this.toggleMute());

        this.isMuted = true;

        this.bgMusic.volume = 0.1;
        this.addSound.volume = 0.2;
        this.deleteSound.volume = 0.2;
    }

    // Method to toggle the mute status
    toggleMute() {
        this.isMuted = !this.isMuted;
        this.crossButton.classList.toggle('hidden');

        if (this.isMuted) {
            this.bgMusic.pause();
        } else {
            this.bgMusic.play();
        }
    }

    // Method to play a given sound element
    playSound(soundElement) {
        if (this.isMuted) return;

        soundElement.currentTime = 0;
        soundElement.play();
    }

    // Method to play the addition sound
    playAddSound() {
        if (this.isMuted) return;

        this.playSound(this.addSound);
        // Stop playing the sound after 1.5 seconds
        setTimeout(() => {
            this.addSound.pause();
          }, 1500);
    }

    // Method to play the deletion sound
    playDeleteSound() {
        if (this.isMuted) return;

        this.playSound(this.deleteSound);
        // Stop playing the sound after 2.5 seconds
        setTimeout(() => {
            this.deleteSound.pause();
          }, 2500);
    }
}