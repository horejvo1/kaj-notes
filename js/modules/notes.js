import {Audio} from './audioAPI.js';

export class Notes {
    notesContainer;
    formContainer;
    addNoteButton;
    notesAndForm;
    audio;
    constructor() {
        this.notesContainer = document.getElementById("notes-container");
        this.formContainer = document.getElementById("form-container");
        this.addNoteButton = document.getElementById("add-note");
        this.notesAndForm = document.getElementById("notes-and-form");

        // Add event listener to the "Add Note" button
        this.addNoteButton.addEventListener("click", () => this.showForm());

        // Add event listener to the form submit event
        document.getElementById('form-note').addEventListener('submit', (event) => this.onFormSubmit(event));

        // Load existing notes from localStorage and display them
        this.getNotes().forEach(note => {
            const noteElement = this.createNoteElementMainPage(note.id, note.content);
            this.notesContainer.appendChild(noteElement);
        });

        // Initialize audio API
        this.audio = new Audio();
    }

    // Retrieve notes from localStorage
    getNotes() {
        return JSON.parse(localStorage.getItem("everydaynotes-notes") || "[]");
    }

    // Save notes to localStorage
    saveNotes(notes) {
        localStorage.setItem("everydaynotes-notes", JSON.stringify(notes));
    }

    // Create a note element for the main page
    createNoteElementMainPage(id, content) {
        const element = document.createElement("div");
        element.classList.add("note");
        element.draggable = true;
        element.id = id;
        const textElement = document.createElement("textarea");
        textElement.value = content;
        textElement.classList.add("note-text");
        textElement.readOnly = true;
        element.appendChild(textElement);
        this.addListenersToNote(element);
        // update note on change
        // open note on double click

        return element;
    }

    // Add a new note
    addNote(content, date, video) {
        const notes = this.getNotes();
        const newNote = {
            id: Math.floor(Math.random() * 100000),
            content: content,
            date: date,
            video: video
        };

        const noteElement = this.createNoteElementMainPage(newNote.id, newNote.content);
        this.notesContainer.appendChild(noteElement);

        notes.push(newNote);
        this.saveNotes(notes);
        // location.reload();
    }

    // Update an existing note
    updateNote(id, newContent) {
        const notes = this.getNotes();
        const targetNote = notes.find(note => note.id == id);
        const noteElement = document.getElementById(id);
        const textElement = noteElement.querySelector("textarea");
        textElement.value = newContent;
        targetNote.content = newContent;
        this.saveNotes(notes);
    }

    // Delete a note
    deleteNote(id, element) {
        const notes = this.getNotes().filter(note => note.id != id);
        this.saveNotes(notes);
        this.notesContainer.removeChild(element);
        this.audio.playDeleteSound();
    }

    // Show the note form
    showForm() {
        this.closeNoteFullScreen();
        this.notesContainer.style.display = "none";
        this.formContainer.style.display = "flex";
    }

    // Handle form submission
    onFormSubmit(event) {
        event.preventDefault();

        const noteText = document.getElementById('note-text').value;
        const noteDate = document.getElementById('note-date').value;
        const noteVideo = document.getElementById('note-video').value;

        document.getElementById('note-text').value = '';
        document.getElementById('note-date').value = '';
        document.getElementById('note-video').value = '';

        this.addNote(noteText, noteDate, noteVideo);

        this.notesContainer.style.display = "grid";
        this.formContainer.style.display = "none";
        this.audio.playAddSound();
    }

    // Show a full note
    // Create new html structure and appends it to the HTML container
    showFullNote(id) {
        const notes = this.getNotes();
        const targetNote = notes.find(note => note.id == id);
        this.notesContainer.style.display = "none";
        this.formContainer.style.display = "none";
        const fullNote = document.createElement("div");
        fullNote.id = "fullNote";

        const labelElement = document.createElement("label");
        labelElement.textContent = "Text vaší poznámky:";
        fullNote.appendChild(labelElement);
        const textElement = document.createElement("textarea");
        textElement.value = targetNote.content;
        fullNote.appendChild(textElement);

        textElement.addEventListener("change", () => {
            this.updateNote(id, textElement.value);
        });

        if (targetNote.date !== "") {
            const labelElement2 = document.createElement("label");
            labelElement2.textContent = "Datum spojené s poznámkou:";
            fullNote.appendChild(labelElement2);
            const dateElement = document.createElement("span");
            dateElement.innerHTML = targetNote.date;
            fullNote.appendChild(dateElement);
        }

        if (targetNote.video !== "") {
            const labelElement3 = document.createElement("label");
            labelElement3.textContent = "Odkaz na video";
            fullNote.appendChild(labelElement3);
            const linkElement = document.createElement("a");
            linkElement.href = targetNote.video;
            linkElement.textContent = "Klikněte zde pro zobrazení připojeného videa";
            linkElement.classList.add("video-link");
            fullNote.appendChild(linkElement);
        }

        const closeButton = document.createElement('button');
        closeButton.textContent = 'Close';
        closeButton.addEventListener('click', this.closeNoteFullScreen);
        closeButton.classList.add('close-button');
        fullNote.appendChild(closeButton);
        this.notesAndForm = document.getElementById("notes-and-form");
        this.notesAndForm.appendChild(fullNote);
    }

    // Close the full note view
    closeNoteFullScreen() {
        const fullNote = document.getElementById('fullNote');
        if (fullNote) {
            this.notesAndForm = document.getElementById("notes-and-form");
            this.notesAndForm.removeChild(fullNote);
            this.notesContainer = document.getElementById("notes-container");
            this.notesContainer.style.display = 'grid';
        }
    }

    // Add event listeners to a note element
    addListenersToNote(element) {
        element.addEventListener("dblclick", () => {
            this.showFullNote(element.getAttribute("id"));
        });

        // delete note on delete key
        element.addEventListener("mouseover", () => {
            window.onkeydown = (event) => {
                if (event.key === 'Delete') {
                    const doDelete = confirm("Opravdu chcete smazat poznámku?");
                    if (doDelete) {
                        this.deleteNote(element.getAttribute("id"), element);
                    }
                }
            };
        });

        element.addEventListener("mouseout", () => {
            window.onkeydown = null;
        });
    }

    // Remove all notes from the HTML container
    removeAllNotesFromHTML() {
        this.notesContainer.innerHTML = "";
    }

    // Add all notes to the HTML container
    addAllNotesToHTML() {
        this.getNotes().forEach(note => {
            const noteElement = this.createNoteElementMainPage(note.id, note.content);
            this.notesContainer.appendChild(noteElement);
            // this.addListenersToNote();
        });
    }
}