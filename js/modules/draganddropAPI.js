// Exporting NotesManager class
export class NotesManager {
    // The constructor is called when a new instance of the class is created
    constructor() {
        this.notes = document.querySelectorAll('.note');
        this.draggedElement = null;
        this.initializeDragAndDrop();
        this.notesLocalStorage = this.getNotes();
    }

    // Method to attach event listeners to the note elements for drag and drop functionality
    initializeDragAndDrop() {
        this.notes.forEach(note => {
            // When the user starts dragging the note, handle it with handleDragStart
            note.addEventListener('dragstart', (e) => this.handleDragStart(e, note), false);
            // While the dragged note is over another note, handle it with handleDragOver
            note.addEventListener('dragover', this.handleDragOver.bind(this), false);
            // When the dragged note is dropped on another note, handle it with handleDrop
            note.addEventListener('drop', (e) => this.handleDrop(e, note), false);
        });
    }

    // Method to handle the start of a drag event
    handleDragStart(e, element) {
        this.draggedElement = element;
        // Allow this event to be moved
        e.dataTransfer.effectAllowed = 'move';
        // Set the data transfer to hold the value of the note's textarea
        e.dataTransfer.setData('text/plain', element.querySelector('textarea').value);
    }
    
    // Method to handle when a dragged note is over another note
    handleDragOver(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        // Set the dropEffect to move, indicating that the dragged note can be moved here
        e.dataTransfer.dropEffect = 'move';
        return false;
    }

    // Method to handle when a dragged note is dropped on another note
    handleDrop(e, element) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        // If the note was dropped on a different note
        if (this.draggedElement !== element) {
            // Swap the values of the two notes' textarea
            this.draggedElement.querySelector('textarea').value = element.querySelector('textarea').value;
            let replacedItem = e.dataTransfer.getData('text/plain');
            element.querySelector('textarea').value = replacedItem;
            // Swap their ids as well
            this.draggedElement.id = element.id;
            element.id = this.draggedElement.id;
        }
        element.classList.remove('over');
    }
    
    // Method to get the notes from local storage
    getNotes() {
        return JSON.parse(localStorage.getItem("everydaynotes-notes") || "[]");
    }

    // Method to save the notes to local storage
    saveNotes(notes) {
        localStorage.setItem("everydaynotes-notes", JSON.stringify(notes));
    }
}